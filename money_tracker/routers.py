from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token
from rest_framework_nested import routers

from .apps.core import views as core_views

router = routers.DefaultRouter()

router.register('users', core_views.UserViewset)

urlpatterns = router.urls
urlpatterns += [
    path('auth/', obtain_jwt_token, name='auth'),
    path('auth/verify/', verify_jwt_token, name='auth-verify'),
]
