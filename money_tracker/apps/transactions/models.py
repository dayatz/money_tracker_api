from django.db import models
from django.contrib.auth.models import User

from money_tracker.apps.accounts.models import BankAccount
from money_tracker.apps.core.models import Category


class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    account = models.ForeignKey(BankAccount, on_delete=models.PROTECT)

    category = models.ForeignKey(Category, null=True,
                                 on_delete=models.SET_NULL)
    expense = models.BooleanField(default=True)
    amount = models.FloatField()
    description = models.TextField(null=True, blank=True)
    date = models.DateField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}: {} {}'.format(self.user, self.amount, self.date)

    class Meta:
        ordering = ['-date']
