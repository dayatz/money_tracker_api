from django.db import models
from django.contrib.auth.models import User


class BankAccount(models.Model):
    user = models.ForeignKey(User, related_name='banks',
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    owner = models.CharField(max_length=100)
    number = models.CharField(max_length=50)

    def __str__(self):
        return '{}: {}'.format(self.user.username, self.name)
