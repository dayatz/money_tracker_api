from django.urls import reverse

from rest_framework import test, status

from money_tracker.apps.core.tests import TestMixinSetup
from .models import BankAccount


class AccountsAPITest(TestMixinSetup, test.APITestCase):
    def setUp(self):
        super().setUp()
        self.account = BankAccount.objects.create(
            user=self.user,
            name='bank-name',
            owner=self.user.username,
            number='123',
        )

    def test_create_account(self):
        self.auth_user()
        url = reverse('api:account-list')
        data = {
            'name': 'Bank Name',
            'number': '123',
            'owner': 'Owner'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_account_list(self):
        self.auth_user()
        url = reverse('api:account-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_account_list_without_auth(self):
        self.client.credentials()
        url = reverse('api:account-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_account_delete(self):
        self.auth_user()
        url = reverse('api:account-delete', kwargs={'pk': self.account.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
