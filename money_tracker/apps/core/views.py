from django.contrib.auth.models import User
from rest_framework import viewsets, permissions, response, status
from rest_framework.decorators import action
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from . import serializers, permissions as perms


def custom_jwt_response_with_user(token, user=None, request=None):
    serializer = serializers.UserSerializer(user, context={'request': request})
    return {
        'token': token,
        'user': serializer.data
    }


class UserViewset(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.exclude(is_superuser=True)
    permission_classes = (perms.UserPermission,)
    authentication_classes = (JSONWebTokenAuthentication,)

    @action(methods=['get'], detail=False,
            permission_classes=(permissions.IsAuthenticated,),
            authentication_classes=(JSONWebTokenAuthentication,))
    def me(self, request):
        serializer = serializers.UserSerializer(
            request.user, context={'request': request})
        return response.Response(serializer.data)

    @action(methods=['post'], detail=True,
            permission_classes=(permissions.IsAuthenticated,),
            authentication_classes=(JSONWebTokenAuthentication,))
    def change_password(self, request, pk):
        serializer = serializers.ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            obj = self.get_object()
            if obj.check_password(
                    serializer.validated_data.get('old_password')):
                obj.set_password(serializer.validated_data.get('new_password'))
                obj.save()
                return response.Response(
                    {'msg': 'change success'}, status=status.HTTP_200_OK
                )
            return response.Response(
                {'msg': 'wrong password'}, status=status.HTTP_400_BAD_REQUEST
            )
        return response.Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)
