from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import test, status


class TestMixinSetup(object):
    def create_user_to_db(self):
        user = User.objects.create_user(
            self.username, self.email, self.password)
        self.user = user
        return user

    def auth_user(self):
        data = {
            'username': self.username,
            'password': self.password
        }
        response = self.client.post(reverse('api:auth'), data)
        self.token = response.data['token']
        self.client.credentials(
            HTTP_AUTHORIZATION='JWT ' + self.token)
        self.user_id = response.data['user']['id']
        self.token = response.data['token']

    def setUp(self):
        self.username = 'user'
        self.email = 'user@email.com'
        self.password = 'password123'
        self.user = self.create_user_to_db()


class ApiAuthTest(TestMixinSetup, test.APITestCase):
    def test_user_create(self):
        data = {
            'username': 'new_user',
            'password': self.password,
            'email': 'new_user@email.com',
        }
        url = reverse('api:user-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_list(self):
        self.auth_user()
        url = reverse('api:user-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_without_auth(self):
        self.client.credentials()
        url = reverse('api:user-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_auth_wrong_credentials(self):
        data = {
            'username': 'wrong username',
            'password': 'wrong password'
        }
        url = reverse('api:user-list')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_profile(self):
        self.auth_user()
        url = reverse('api:user-me')
        response = self.client.get(url)
        self.assertEqual(response.data['username'], self.username)
        self.assertEqual(response.data['email'], self.email)

    def test_change_password(self):
        self.auth_user()
        url = reverse('api:user-change-password', kwargs={'pk': self.user_id})
        new_password = 'new_password'
        data = {
            'old_password': self.password,
            'new_password': new_password
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = {
            'username': self.username,
            'password': new_password
        }
        response = self.client.post(reverse('api:auth'), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)

    def test_change_password_wrong(self):
        self.auth_user()
        url = reverse('api:user-change-password', kwargs={'pk': self.user_id})
        data = {
            'old_password': 'wrong password',
            'new_password': 'new_password'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
