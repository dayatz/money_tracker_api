from django.contrib.auth.models import User

from rest_framework import serializers, validators


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True)
    new_password = serializers.CharField(write_only=True)


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(
        max_length=20,
        validators=[validators.UniqueValidator(queryset=User.objects.all())])
    email = serializers.EmailField(
        validators=[validators.UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)
    # fullname = serializers.CharField(max_length=50, write_only=True)
    # phone = serializers.CharField(max_length=20, write_only=True)

    # userprofile = UserProfileSerializer(read_only=True)
    is_superuser = serializers.BooleanField(read_only=True)
    # photo = serializers.ImageField(required=False)
    # status = serializers.CharField(max_length=10, write_only=True)

    # def validate_phone(self, value):
    #     if self.instance and models.UserProfile.objects.exclude(
    #             user=self.instance).filter(phone=value).exists():
    #         raise serializers.ValidationError('This field must be unique.')
    #     return value

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        # models.UserProfile.objects.create(
        #     user=user, phone=validated_data['phone'],
        #     fullname=validated_data['fullname'],
        #     status=validated_data['status'])
        return user

    # def update(self, instance, validated_data):
        # userprofile = instance.userprofile
        # userprofile.fullname = validated_data.get('fullname',
        #                                           userprofile.fullname)
        # userprofile.phone = validated_data.get('phone', userprofile.phone)
        # userprofile.photo = validated_data.get('photo', userprofile.photo)
        # userprofile.save()
        # return instance
