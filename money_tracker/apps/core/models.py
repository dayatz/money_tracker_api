from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='profile')
    fullname = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.user


class Category(models.Model):
    title = models.CharField(max_length=100)
    expense = models.BooleanField(default=True)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
